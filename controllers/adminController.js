const express = require("express");
const router = express.Router();
const { body, validationResult } = require("express-validator");
var database = require("../db/database");

var jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

// we need to kept this JWT_SECRET in environment vairable but kept here because this website is only for demo.
const JWT_SECTRT = "carkey";

router.post("/create_new_car", async (req, res) => {
  try {
    const data = req.body;
    const [rows_table] = await database.execute(`SHOW TABLES LIKE 'new_car'`);
    if (rows_table.length <= 0) {
      const [rows] = await database.query(`CREATE TABLE new_car (
              id INT PRIMARY KEY AUTO_INCREMENT,
              vehicle_model VARCHAR(50) NOT NULL,
              vehicle_number VARCHAR(50) NOT NULL,
              rent_per_day INT NOT NULL,
              seating_capacity INT NOT NULL,
              UNIQUE (vehicle_number)
              )`);
    }
    let car = await database.query(
      `SELECT * FROM new_car WHERE vehicle_number='${req.body.vehicle_number}'`
    );
    if (car[0].length > 0) {
      return res.status(400).json({
        error: true,
        message: "Sorry a car with this number or model already exists",
      });
    }
    const [rows1] = await database.query(
      `INSERT INTO new_car (vehicle_model, vehicle_number, rent_per_day, seating_capacity) VALUES ('${data.vehicle_model}', '${data.vehicle_number}', '${data.rent_per_day}', '${data.seating_capacity}')`
    );
    res.status(200).json({
      error: false,
      message: "Car Data Insertded Successfully",
      data: rows1,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/buy_car", async (req, res) => {
  try {
    const data = req.body;
    if (data.date == "" || data.no_of_days <= 0) {
      return res.status(400).json({
        error: true,
        message: "Sorry Please fill all field",
      });
    }
    const decodedToken = jwt.verify(req.body.userIdToken, JWT_SECTRT);
    const [rows_table] = await database.execute(`SHOW TABLES LIKE 'buy_car'`);
    if (rows_table.length <= 0) {
      const [rows] = await database.query(`CREATE TABLE buy_car (
              id INT PRIMARY KEY AUTO_INCREMENT,
              userId VARCHAR(50) NOT NULL,
              car_id INT NOT NULL,
              car_number VARCHAR(50) NOT NULL,
              date VARCHAR(50) NOT NULL,
              no_of_days INT NOT NULL
              )`);
    }
    let car = await database.query(
      `SELECT * FROM buy_car WHERE car_id='${req.body.car_id}'`
    );
    if (car[0].length > 0) {
      return res.status(400).json({
        error: true,
        message: "Sorry a car with this number or model already buy",
      });
    }
    let car_number = await database.query(
      `SELECT vehicle_number FROM new_car WHERE id='${data.car_id}'`
    );
    console.log(car_number[0][0].vehicle_number, "car_number");
    const [rows1] = await database.query(
      `INSERT INTO buy_car (car_id, userId, car_number, date, no_of_days) VALUES ('${data.car_id}', '${decodedToken.id}', '${car_number[0][0].vehicle_number}', '${data.date}', '${data.no_of_days}')`
    );
    res
      .status(200)
      .json({ error: false, message: "Car Buy Successfully", data: rows1 });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/delete_car", async (req, res) => {
  try {
    const data = req.body;

    const [rows1] = await database.query(
      `DELETE FROM new_car WHERE id = ${data.id};`
    );
    res.status(200).json({
      error: false,
      message: "Car Data Deleted Successfully",
      data: rows1,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/edit_car", async (req, res) => {
  try {
    const data = req.body;

    const [rows] = await database.query(
      `SELECT * FROM new_car
        WHERE id = ${data.id};`
    );
    let carDetail = rows[0];
    const [rows1] = await database.query(
      `UPDATE new_car
      SET 
      vehicle_model =  '${
        data.vehicle_model ? data.vehicle_model : carDetail.vehicle_model
      }',
      vehicle_number = '${
        data.vehicle_number ? data.vehicle_number : carDetail.vehicle_number
      }',
      rent_per_day =   '${
        data.rent_per_day ? data.rent_per_day : carDetail.rent_per_day
      }',
      seating_capacity = '${
        data.seating_capacity
          ? data.seating_capacity
          : carDetail.seating_capacity
      }'
      WHERE id = ${data.id};`
    );
    res.status(200).json({
      error: false,
      message: "Car Data Deleted Successfully",
      data: rows1,
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/get_all_car", async (req, res) => {
  try {
    let allCar;
    console.log(req.body.search_value,"y");
    if(req.body.search_value)
    {
       allCar = await database.query(`SELECT * FROM new_car
       WHERE vehicle_number LIKE '${req.body.search_value}%'`);
    }
    else
    {
       allCar = await database.query(`SELECT * FROM new_car`);
    }
    res.status(200).json({
      error: false,
      message: "Car Data Fetched Successfully",
      data: allCar[0],
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/get_rent_car", async (req, res) => {
  try {
    if(req.body.search_value)
    {
       allCar = await database.query(`SELECT * FROM buy_car
       WHERE car_number LIKE '${req.body.search_value}%'`);
    }
    else
    {
       allCar = await database.query(`SELECT * FROM buy_car`);
    }
    res.status(200).json({
      error: false,
      message: "Car Data Fetched Successfully",
      data: allCar[0],
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

router.post("/get_user_rent_car", async (req, res) => {
  try {
    let allCar
    const decodedToken = jwt.verify(req.body.userIdToken, JWT_SECTRT);
    if(req.body.search_value)
    {
       allCar = await database.query(
        `SELECT * FROM buy_car WHERE userId='${decodedToken.id}' && car_number LIKE '${req.body.search_value}%'`
      );
    }
    else
    {
       allCar = await database.query(`SELECT * FROM buy_car WHERE userId='${decodedToken.id}'`);
    }
    res.status(200).json({
      error: false,
      message: "Car Data Fetched Successfully",
      data: allCar[0],
    });
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({ error: true, message: "internal server Error occured" });
  }
});

module.exports = router;
