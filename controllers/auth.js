const express = require("express");
const router = express.Router();
const { body, validationResult } = require("express-validator");
var database = require("../db/database");
var jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

const JWT_SECTRT = "carkey";

router.post(
  "/signup",
  [
    body("name", "Enter a valid name").isLength({ min: 3 }),
    body("email", "Enter a valid email")
      .notEmpty()
      .withMessage("Email is required")
      .isEmail()
      .withMessage("Invalid email format"),
    body("password").isLength({ min: 5 }),
    body("confirmpassword").isLength({ min: 5 }),
  ],
  async (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res
          .status(400)
          .json({ error: true, message: errors.array()[0].msg });
      }
      const data = req.body;
      const [rows_table] = await database.execute(
        `SHOW TABLES LIKE 'car_admin'`
      );
      if (rows_table.length <= 0) {
        const [rows] = await database.query(`CREATE TABLE car_admin (
              id INT PRIMARY KEY AUTO_INCREMENT,
              name VARCHAR(50) NOT NULL,
              email VARCHAR(50),
              isAdmin INT DEFAULT 0,
              password VARCHAR(500) NOT NULL,
              UNIQUE (email)
              )`);
      }
      let user = await database.query(
        `SELECT email FROM car_admin WHERE email='${req.body.email}'`
      );
      if (user[0].length > 0) {
        return res.status(400).json({
          error: true,
          message: "Sorry a user with this email already exists",
        });
      }
      if (req.body.password != req.body.confirmpassword) {
        return res
          .status(400)
          .json({ error: true, message: "Sorry! Password is not matched" });
      }
      const salt = await bcrypt.genSalt(10);
      secPass = await bcrypt.hash(req.body.password, salt);
      const [rows1] = await database.query(
        `INSERT INTO car_admin (name, email, isAdmin, password) VALUES ('${data.name}', '${data.email}', '${data.isAdmin}', '${secPass}')`
      );
      res
        .status(200)
        .json({ error: false, message: "Register Successfully", data: rows1 });
    } catch (error) {
      console.error(error.message);
      res
        .status(500)
        .send({ error: true, message: "internal server Error occured" });
    }
  }
);

router.post(
  "/login",
  [
    body("email", "Enter valid Email").isEmail(),
    body("password", "Password cannot be blank").exists(),
  ],
  async (req, res) => {
    const data = req.body;
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res
        .status(400)
        .json({ error: true, message: errors.array()[0].msg });
    }

    try {
      let user = await database.query(
        `SELECT * FROM car_admin WHERE email='${data.email}'`
      );
      user = user[0][0];
      if (!user) {
        success = false;
        return res.status(400).json({
          error: true,
          message: "Please try to login with correct credential",
        });
      }
      const passwordCompare = await bcrypt.compare(
        data.password,
        user.password
      );
      if (!passwordCompare) {
        success = false;
        return res.status(400).json({
          error: true,
          message: "Please try to login with correct password",
        });
      }
      const data1 = {
        id: user.email,
        admin: user.isAdmin,
      };
      const authtoken = jwt.sign(data1, JWT_SECTRT);
      res.json({
        error: false,
        authtoken: authtoken,
        email: user.email,
        isAdmin: user.isAdmin,
        message: "Login Succesfully",
      });
      // res.status(success).json(authtoken);
    } catch (error) {
      console.error(error.message);
      res
        .status(500)
        .send({
          error: true,
          error_message: error,
          message: "internal server Error occured",
        });
    }
  }
);

router.post("/get_id_by_token", async (req, res) => {
  try {
    const decodedToken = jwt.verify(req.body.authtoken, JWT_SECTRT);
    let user = await database.query(
      `SELECT name FROM car_admin WHERE email='${decodedToken.id}'`
    );
    console.log(user[0][0].name, "user");
    res.json({
      error: false,
      id: decodedToken.id,
      name: user[0][0].name,
      isAdmin: decodedToken.admin,
    });
    // res.status(success).json(authtoken);
  } catch (error) {
    console.error(error.message);
    res
      .status(500)
      .send({
        error: true,
        error_message: error,
        message: "internal server Error occured",
      });
  }
});

module.exports = router;
